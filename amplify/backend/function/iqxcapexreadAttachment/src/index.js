/* Amplify Params - DO NOT EDIT
    API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT
    API_IQXCAPEX_GRAPHQLAPIIDOUTPUT
    API_IQXCAPEX_GRAPHQLAPIKEYOUTPUT
    AUTH_IQXCAPEX74967E90_USERPOOLID
    ENV
    REGION
    STORAGE_IQXCAPEXDEVSTORAGE_BUCKETNAME
Amplify Params - DO NOT EDIT */

const AWS = require('aws-sdk');
// AWS.config.update({
//     accessKeyId: process.env.AWS_ACCESS_KEY_ID,
//     secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
//     region: process.env.REGION,
//     signatureVersion: 'v4'
// });
const AWSAppSyncClient = require('aws-appsync').default;
const gql = require('graphql-tag');
global.fetch = require('node-fetch');

let graphqlClient, graphql_auth;

// READ ATTACHMENT

exports.handler = async (event) => {

    //console.log("ENV:", process.env)
    //console.log("EVENT:", event)

    //are we on AWS or local mock?
    if ('AWS_EXECUTION_ENV' in process.env && (process.env.AWS_EXECUTION_ENV.indexOf('-mock') <= -1)) {
        //for cloud env
        env = process.env;
        graphql_auth = {
            type: "AWS_IAM",
            credentials: {
                accessKeyId: env.AWS_ACCESS_KEY_ID,
                secretAccessKey: env.AWS_SECRET_ACCESS_KEY,
                sessionToken: env.AWS_SESSION_TOKEN,
            }
        };
    } else {
        console.log("local mock")
        // for local mock
        env = {
            API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT: 'http://192.168.0.181:20002/graphql',
            REGION: 'us-east-1',
        }
        graphql_auth = {
            type: "AWS_IAM",
            credentials: {
                accessKeyId: 'mock',
                secretAccessKey: 'mock',
                sessionToken: 'mock',
            }
        };
    }

    let attachment = event.arguments.input.attachment;
    let request = event.arguments.input.requestId;

    
    if (attachment.identity && request && event.identity.username) {
        try {

            // read Request and ensure that current user is an agent
            if (!graphqlClient) {
                graphqlClient = new AWSAppSyncClient({
                    url: env.API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT,
                    region: env.REGION,
                    auth: graphql_auth,
                    disableOffline: true,
                });
            }
        
            const readInput = {
                query: gql(readRequest),
                variables: {
                    id: request 
                },
            };
            const res = await graphqlClient.query(readInput);

            //console.log("RES", res)

            //ensure that current user is an agent
            let foundUser = null;
            if(res.data.getRequest.agents.length){
                foundUser = res.data.getRequest.agents.find((u)=>{
                    return u === event.identity.username;
                })
            }
            console.log("found user", foundUser)
            if(foundUser === null || foundUser === undefined){
                return Promise.reject(new Error("unauthorized"));
            } 

            // ok, user is agent - get signed URL of attachment
            AWS.config.update({
//                accessKeyId: process.env.AWS_ACCESS_KEY_ID,
//                secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
                region: process.env.REGION,
                signatureVersion: 'v4'
            });
            let s3 = new AWS.S3()

            let url = s3.getSignedUrl('getObject', {
                Bucket: process.env.STORAGE_IQXCAPEXDEVSTORAGE_BUCKETNAME,
                Key: 'protected/' + attachment.identity + '/' + attachment.key
            })

            console.log(url)
            attachment.url = url;

            // return the Attachment
            return Promise.resolve(attachment);

        } catch (error) {
            console.log(error)
            return Promise.reject(new Error(error));
         }
    }

};

// GraphQL Statements ------------------------------------------------
const readRequest = /* GraphQL */ `
 query GetRequest($id: ID!) {
  getRequest(id: $id) {
    id
    organizationID
    agents
  }
}
`;

