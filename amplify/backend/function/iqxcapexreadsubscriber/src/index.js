/* Amplify Params - DO NOT EDIT
    API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT
    API_IQXCAPEX_GRAPHQLAPIIDOUTPUT
    API_IQXCAPEX_GRAPHQLAPIKEYOUTPUT
    ENV
    REGION
Amplify Params - DO NOT EDIT */

//  READ SUBSCRIBER
const AWSAppSyncClient = require('aws-appsync').default;
const gql = require('graphql-tag');
global.fetch = require('node-fetch');

let graphqlClient, graphql_auth;

exports.handler = async (event) => {

    console.log("EVENT", event)

    //are we on AWS or local mock?
    if ('AWS_EXECUTION_ENV' in process.env && (process.env.AWS_EXECUTION_ENV.indexOf('-mock') <= -1)) {
        //for cloud env
        env = process.env;
        graphql_auth = {
            type: "AWS_IAM",
            credentials: {
                accessKeyId: env.AWS_ACCESS_KEY_ID,
                secretAccessKey: env.AWS_SECRET_ACCESS_KEY,
                sessionToken: env.AWS_SESSION_TOKEN,
            }
        };
    } else {
        console.log("local mock")
        // for local mock
        env = {
            API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT: 'http://192.168.0.181:20002/graphql',
            REGION: 'us-east-1',
        }
        graphql_auth = {
            type: "AWS_IAM",
            credentials: {
                accessKeyId: 'mock',
                secretAccessKey: 'mock',
                sessionToken: 'mock',
            }
        };
    }

    //read Subscriber based on host name
    if (!graphqlClient) {
        graphqlClient = new AWSAppSyncClient({
            url: env.API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT,
            region: env.REGION,
            auth: graphql_auth,
            disableOffline: true,
        });
    }

    let host = 'iqx';   //temp!!
    const readInput = {
        query: gql(readSubscriber),
        variables: {
            subdomain: host
        },
    };
    const res = await graphqlClient.query(readInput);
    console.log("SUBSCRIBER", res);

    return Promise.resolve(res.data.getSubscriber);
};

const readSubscriber = /* GraphQL */ `
 query ReadSubscriber($subdomain: String!) {
   getSubscriber(subdomain: $subdomain) {
    subdomain   
    organizationID
    user_pools_id
    app_client_id
  }
}
`;
