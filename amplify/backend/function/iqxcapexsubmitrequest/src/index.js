/* Amplify Params - DO NOT EDIT
    API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT
    API_IQXCAPEX_GRAPHQLAPIIDOUTPUT
    API_IQXCAPEX_GRAPHQLAPIKEYOUTPUT
    AUTH_IQXCAPEX74967E90_USERPOOLID
    ENV
    REGION
Amplify Params - DO NOT EDIT *//* Amplify Params - DO NOT EDIT */

const AWS = require('aws-sdk');
const AWSAppSyncClient = require('aws-appsync').default;
const gql = require('graphql-tag');
global.fetch = require('node-fetch');

AWS.config.region = process.env.REGION;
let graphqlClient, graphql_auth;


exports.handler = async (event) => {

    console.log("ENV:", process.env)
    // console.log("EVENT:", event)

    //are we on AWS or local mock?
    if ('AWS_EXECUTION_ENV' in process.env && (process.env.AWS_EXECUTION_ENV.indexOf('-mock') <= -1)) {
        //for cloud env
        env = process.env;
        graphql_auth = {
            type: "AWS_IAM",
            credentials: {
                accessKeyId: env.AWS_ACCESS_KEY_ID,
                secretAccessKey: env.AWS_SECRET_ACCESS_KEY,
                sessionToken: env.AWS_SESSION_TOKEN,
            }
        };
    } else {
        console.log("local mock")
        // for local mock
        env = {
            API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT: 'http://192.168.0.181:20002/graphql',
            REGION: 'us-east-1',
        }
        graphql_auth = {
            type: "AWS_IAM",
            credentials: {
                accessKeyId: 'mock',
                secretAccessKey: 'mock',
                sessionToken: 'mock',
            }
        };
    }

    // determine email address of user (to add to Request, as primary identifier in FAB)
    var provider = new AWS.CognitoIdentityServiceProvider()

    //first get 'user pool id' from identity
    let issuerComponents = event.identity.issuer.split('/')
    let userPoolId = issuerComponents[issuerComponents.length - 1]

    //get email from user name on identity
    try {
        const user = await provider.adminGetUser({
            UserPoolId: userPoolId, //process.env.AUTH_IQXCAPEX74967E90_USERPOOLID,
            Username: event.identity.username,
        }).promise()

        //       console.log(user.UserAttributes)
        let email = user.UserAttributes.find(att => {
            return att.Name === 'email';
        })
        console.log(email.Value);

        //create Request
        // first create graphQL client
        if (!graphqlClient) {
            graphqlClient = new AWSAppSyncClient({
                url: env.API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT,
                region: env.REGION,
                auth: graphql_auth,
                disableOffline: true,
            });
        }

        //next, determine approvers/agents (temp!)
        let userList = await provider.listUsers({
            UserPoolId: userPoolId,    // process.env.AUTH_IQXCAPEX74967E90_USERPOOLID
            AttributesToGet: ["sub"],
            Filter: "email = \"adam.miles@iqxbusiness.com\""
        }).promise()

        //    console.log(userList);
        let agents = []
        for (let u in userList.Users) {
            let agent = userList.Users[u].Username;
            agents.push(agent);
        }
        //console.log(agents)

        //create the Request
        // ( set status to 'Submitted' and set 'sendToSAP' to the current OrgId so that it can be 
        // 'pulled' into SAP as new FAB Request )
        const postInput = {
            mutation: gql(createRequest),
            variables: {
                input: {
                    companyCode: event.arguments.header.companyCode,
                    investmentType: event.arguments.header.investmentType,
                    description: event.arguments.header.description,
                    amount: event.arguments.header.amount,
                    organizationID: event.arguments.header.organizationID,
                    attachments: event.arguments.header.attachments,
                    status: 'Submitted',
                    submittedByEmail: email.Value,
                    sendToSAP: event.arguments.header.organizationID,
                    owner: event.identity.username,
                    agents: agents
                },
            },
        };
        const res = await graphqlClient.mutate(postInput);
        let request = res.data.createRequest;

        // add to 'Approvers' table, so that a user can view the Requests they are responsible for
        for (let a in agents) {
            let agent = agents[a];
            
            let postApprover = {
                mutation: gql(createApprover),
                variables: {
                    input: {
                        user: agent,
                        organizationID: event.arguments.header.organizationID,
                        approverRequestToApproveId: request.id
                     },
                },
            };
            const res2 = await graphqlClient.mutate(postApprover);
        }

        // return the Request
        console.log(request.id)
        return Promise.resolve(request);

    } catch (error) {
        return Promise.reject(error)

    }

};

// GraphQL Statements ------------------------------------------------
const createRequest = /* GraphQL */ `
 mutation CreatRequest(
  $input: CreateRequestInput!
  $condition: ModelRequestConditionInput
) {
  createRequest(input: $input, condition: $condition) {
    id
    organizationID
    createdAt
    updatedAt    
    owner
    sendToSAP
    companyCode
    investmentType
    description
    amount
    status
    submittedByEmail
    zinstance
  }
}
`;

const createApprover = /* GraphQL */ `
mutation CreateApprover(
    $input: CreateApproverInput!
    $condition: ModelApproverConditionInput
  ) {
    createApprover(input: $input, condition: $condition) {
      id
      user
      organizationID
      workflowInstanceId
      createdAt
      updatedAt
      RequestToApprove {
        id
        owner
        sendToSAP
        companyCode
        investmentType
        description
        amount
        status
        submittedByEmail
        organizationID
        zinstance
        attachments {
          fileName
          fileSize
          fileType
          key
          identity
          url
        }
        agents
        createdAt
        updatedAt
      }
    }
  }
`;