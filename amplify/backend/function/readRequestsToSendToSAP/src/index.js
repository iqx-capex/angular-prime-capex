/* Amplify Params - DO NOT EDIT
    API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT
    API_IQXCAPEX_GRAPHQLAPIIDOUTPUT
    API_IQXCAPEX_GRAPHQLAPIKEYOUTPUT
    AUTH_IQXCAPEX74967E90_USERPOOLID
    ENV
    REGION
    STORAGE_IQXCAPEXDEVSTORAGE_BUCKETNAME
Amplify Params - DO NOT EDIT */

//  READ REQUESTS TO SEND TO SAP
const AWS = require('aws-sdk');
//AWS.config.region = process.env.REGION;
AWS.config.update({
//    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
//    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.REGION,
    signatureVersion: 'v4'
});

const AWSAppSyncClient = require('aws-appsync').default;
const gql = require('graphql-tag');
global.fetch = require('node-fetch');

let graphqlClient, graphql_auth;

exports.handler = async (event) => {

    //TO DO - validate apiKey against OrgId
    console.log("apiKey", event.request.headers['x-api-key'])
    let orgId = event.arguments.input.organizationID
    console.log("orgId", orgId)
    console.log("accessKeyId", process.env.AWS_ACCESS_KEY_ID)
    console.log("secretAccessKey", process.env.AWS_SECRET_ACCESS_KEY)
    console.log("sessionToken",  process.env.AWS_SESSION_TOKEN)

    //are we on AWS or local mock?
    if ('AWS_EXECUTION_ENV' in process.env && (process.env.AWS_EXECUTION_ENV.indexOf('-mock') <= -1)) {
        //for cloud env
        env = process.env;
        graphql_auth = {
            type: "AWS_IAM",
            credentials: {
                accessKeyId: env.AWS_ACCESS_KEY_ID,
                secretAccessKey: env.AWS_SECRET_ACCESS_KEY,
                sessionToken: env.AWS_SESSION_TOKEN,
            }
        };

    } else {
        console.log("local mock")
        // for local mock
        env = {
            API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT: 'http://192.168.0.181:20002/graphql',
            REGION: 'us-east-1',
        }
        graphql_auth = {
            type: "AWS_IAM",
            credentials: {
                accessKeyId: 'mock',
                secretAccessKey: 'mock',
                sessionToken: 'mock',
            }
        };
    }

    if (!graphqlClient) {
        graphqlClient = new AWSAppSyncClient({
            url: env.API_IQXCAPEX_GRAPHQLAPIENDPOINTOUTPUT,
            region: env.REGION,
            auth: graphql_auth,
            disableOffline: true,
        });
    }

    const readInput = {
        query: gql(readRequestsToSendToSAP),
        variables: {
            sendToSAP: orgId //'cec17c82-feee-4ce0-b8d9-3bbd3a614cfd'   //TEMP! 'c7c41195-3c00-4885-bdb9-a361933008e7' 
        },
    };
    const res = await graphqlClient.query(readInput);
    //console.log("RES", res)




    for (let i in res.data.requestsToSendToSAP.items) {
        let item = res.data.requestsToSendToSAP.items[i]
        //console.log(item.description);
        if (item.attachments) {


            for (let a in item.attachments) {
                let attachment = item.attachments[a]
                //console.log("key", attachment.key)
                //console.log("identity", attachment.identity)
                if (attachment.identity) {
                    try {
                        let s3 = new AWS.S3()

                        let url = s3.getSignedUrl('getObject', {
                            Bucket: process.env.STORAGE_IQXCAPEXDEVSTORAGE_BUCKETNAME,
                            // Key: 'private/ap-southeast-2:a38d5253-3a0c-4aaa-936e-fe326cb18fa0/c7c41195-3c00-4885-bdb9-a361933008e7/45e32140-b6ee-4f2d-8257-e26725a8f2ae_ALTMP_IQX_6.PDF' 
                            Key: 'protected/' + attachment.identity + '/' + attachment.key
                    })

                    console.log(url)
                    attachment.url = url;

                    } catch (error) {
                        console.log(error)
                    }
                }
            }
        }

    }
    return Promise.resolve(res.data.requestsToSendToSAP.items);
};

const readRequestsToSendToSAP = /* GraphQL */ `
query RequestsToSendToSap(
  $sendToSAP: String
) {
  requestsToSendToSAP(
    sendToSAP: $sendToSAP
  ) {
    items {
      id
      owner
      sendToSAP
      companyCode
      investmentType
      description
      amount
      status
      submittedByEmail
      organizationID
      zinstance
      attachments {
        fileName
        fileSize
        fileType
        key
        identity
        url
      }
      createdAt
      updatedAt
    }
    nextToken
  }
}
`;
