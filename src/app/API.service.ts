/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation, GraphQLResult } from "@aws-amplify/api-graphql";
import { Observable } from "zen-observable-ts";

export interface SubscriptionResponse<T> {
  value: GraphQLResult<T>;
}

export type CreateSubscriberInput = {
  subdomain: string;
  organizationID?: string | null;
  user_pools_id?: string | null;
  app_client_id?: string | null;
  federated?: boolean | null;
  federated_domain?: string | null;
  redirectSignIn?: string | null;
};

export type ModelSubscriberConditionInput = {
  organizationID?: ModelStringInput | null;
  user_pools_id?: ModelStringInput | null;
  app_client_id?: ModelStringInput | null;
  federated?: ModelBooleanInput | null;
  federated_domain?: ModelStringInput | null;
  redirectSignIn?: ModelStringInput | null;
  and?: Array<ModelSubscriberConditionInput | null> | null;
  or?: Array<ModelSubscriberConditionInput | null> | null;
  not?: ModelSubscriberConditionInput | null;
};

export type ModelStringInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null"
}

export type ModelSizeInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
};

export type ModelBooleanInput = {
  ne?: boolean | null;
  eq?: boolean | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
};

export type Subscriber = {
  __typename: "Subscriber";
  subdomain?: string;
  organizationID?: string | null;
  user_pools_id?: string | null;
  app_client_id?: string | null;
  federated?: boolean | null;
  federated_domain?: string | null;
  redirectSignIn?: string | null;
  createdAt?: string;
  updatedAt?: string;
};

export type UpdateSubscriberInput = {
  subdomain: string;
  organizationID?: string | null;
  user_pools_id?: string | null;
  app_client_id?: string | null;
  federated?: boolean | null;
  federated_domain?: string | null;
  redirectSignIn?: string | null;
};

export type DeleteSubscriberInput = {
  subdomain: string;
};

export type CreateOrganizationInput = {
  id?: string | null;
  name: string;
  companyCodes?: Array<CompanyCodeInput | null> | null;
  workflowDefinition?: WorkflowDefinitionInput | null;
};

export type CompanyCodeInput = {
  code?: string | null;
  description?: string | null;
};

export type WorkflowDefinitionInput = {
  id?: string | null;
  name?: string | null;
  levels?: Array<WorkflowLevelInput | null> | null;
};

export type WorkflowLevelInput = {
  id?: string | null;
  name?: string | null;
  steps?: Array<WorkflowStepInput | null> | null;
};

export type WorkflowStepInput = {
  id?: string | null;
  name?: string | null;
  levelId?: string | null;
  collapsed?: boolean | null;
  assignee?: string | null;
  users?: string | null;
};

export type ModelOrganizationConditionInput = {
  name?: ModelStringInput | null;
  and?: Array<ModelOrganizationConditionInput | null> | null;
  or?: Array<ModelOrganizationConditionInput | null> | null;
  not?: ModelOrganizationConditionInput | null;
};

export type Organization = {
  __typename: "Organization";
  id?: string;
  name?: string;
  companyCodes?: Array<CompanyCode | null> | null;
  workflowDefinition?: WorkflowDefinition;
  createdAt?: string;
  updatedAt?: string;
};

export type CompanyCode = {
  __typename: "CompanyCode";
  code?: string | null;
  description?: string | null;
};

export type WorkflowDefinition = {
  __typename: "WorkflowDefinition";
  id?: string | null;
  name?: string | null;
  levels?: Array<WorkflowLevel | null> | null;
};

export type WorkflowLevel = {
  __typename: "WorkflowLevel";
  id?: string | null;
  name?: string | null;
  steps?: Array<WorkflowStep | null> | null;
};

export type WorkflowStep = {
  __typename: "WorkflowStep";
  id?: string | null;
  name?: string | null;
  levelId?: string | null;
  collapsed?: boolean | null;
  assignee?: string | null;
  users?: string | null;
};

export type DeleteOrganizationInput = {
  id?: string | null;
};

export type UpdateOrganizationInput = {
  id: string;
  name?: string | null;
  companyCodes?: Array<CompanyCodeInput | null> | null;
  workflowDefinition?: WorkflowDefinitionInput | null;
};

export type CreateRequestInput = {
  id?: string | null;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<AttachmentInput | null> | null;
  agents?: Array<string | null> | null;
};

export type AttachmentInput = {
  fileName?: string | null;
  fileSize?: number | null;
  fileType?: string | null;
  key?: string | null;
  identity?: string | null;
  url?: string | null;
};

export type ModelRequestConditionInput = {
  sendToSAP?: ModelStringInput | null;
  companyCode?: ModelStringInput | null;
  investmentType?: ModelStringInput | null;
  description?: ModelStringInput | null;
  amount?: ModelFloatInput | null;
  status?: ModelStringInput | null;
  submittedByEmail?: ModelStringInput | null;
  organizationID?: ModelIDInput | null;
  zinstance?: ModelStringInput | null;
  and?: Array<ModelRequestConditionInput | null> | null;
  or?: Array<ModelRequestConditionInput | null> | null;
  not?: ModelRequestConditionInput | null;
};

export type ModelFloatInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
};

export type ModelIDInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export type Request = {
  __typename: "Request";
  id?: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID?: string;
  zinstance?: string | null;
  attachments?: Array<Attachment | null> | null;
  agents?: Array<string | null> | null;
  createdAt?: string;
  updatedAt?: string;
};

export type Attachment = {
  __typename: "Attachment";
  fileName?: string;
  fileSize?: number | null;
  fileType?: string | null;
  key?: string;
  identity?: string | null;
  url?: string | null;
};

export type UpdateRequestInput = {
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID?: string | null;
  zinstance?: string | null;
  attachments?: Array<AttachmentInput | null> | null;
  agents?: Array<string | null> | null;
};

export type DeleteRequestInput = {
  id: string;
};

export type CreateApproverInput = {
  id?: string | null;
  user: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  approverRequestToApproveId?: string | null;
};

export type ModelApproverConditionInput = {
  organizationID?: ModelIDInput | null;
  workflowInstanceId?: ModelIDInput | null;
  and?: Array<ModelApproverConditionInput | null> | null;
  or?: Array<ModelApproverConditionInput | null> | null;
  not?: ModelApproverConditionInput | null;
};

export type Approver = {
  __typename: "Approver";
  id?: string;
  user?: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  createdAt?: string;
  updatedAt?: string;
  RequestToApprove?: Request;
};

export type UpdateApproverInput = {
  id: string;
  user?: string | null;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  approverRequestToApproveId?: string | null;
};

export type DeleteApproverInput = {
  id?: string | null;
};

export type CreateWorkflowInstanceInput = {
  id?: string | null;
  organizationID: string;
  workflowDefinitionId: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
};

export type ModelWorkflowInstanceConditionInput = {
  organizationID?: ModelIDInput | null;
  workflowDefinitionId?: ModelIDInput | null;
  inProgress?: ModelStringInput | null;
  currentLevel?: ModelStringInput | null;
  currentStep?: ModelStringInput | null;
  and?: Array<ModelWorkflowInstanceConditionInput | null> | null;
  or?: Array<ModelWorkflowInstanceConditionInput | null> | null;
  not?: ModelWorkflowInstanceConditionInput | null;
};

export type WorkflowInstance = {
  __typename: "WorkflowInstance";
  id?: string;
  organizationID?: string;
  workflowDefinitionId?: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
  createdAt?: string;
  updatedAt?: string;
};

export type UpdateWorkflowInstanceInput = {
  id: string;
  organizationID?: string | null;
  workflowDefinitionId?: string | null;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
};

export type DeleteWorkflowInstanceInput = {
  id?: string | null;
};

export type SubmitRequestInput = {
  id?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  organizationID?: string | null;
  attachments?: Array<AttachmentInput | null> | null;
};

export type ModelSubscriberFilterInput = {
  subdomain?: ModelStringInput | null;
  organizationID?: ModelStringInput | null;
  user_pools_id?: ModelStringInput | null;
  app_client_id?: ModelStringInput | null;
  federated?: ModelBooleanInput | null;
  federated_domain?: ModelStringInput | null;
  redirectSignIn?: ModelStringInput | null;
  and?: Array<ModelSubscriberFilterInput | null> | null;
  or?: Array<ModelSubscriberFilterInput | null> | null;
  not?: ModelSubscriberFilterInput | null;
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC"
}

export type ModelSubscriberConnection = {
  __typename: "ModelSubscriberConnection";
  items?: Array<Subscriber | null> | null;
  nextToken?: string | null;
};

export type ModelOrganizationFilterInput = {
  id?: ModelIDInput | null;
  name?: ModelStringInput | null;
  and?: Array<ModelOrganizationFilterInput | null> | null;
  or?: Array<ModelOrganizationFilterInput | null> | null;
  not?: ModelOrganizationFilterInput | null;
};

export type ModelOrganizationConnection = {
  __typename: "ModelOrganizationConnection";
  items?: Array<Organization | null> | null;
  nextToken?: string | null;
};

export type ModelRequestFilterInput = {
  id?: ModelIDInput | null;
  owner?: ModelStringInput | null;
  sendToSAP?: ModelStringInput | null;
  companyCode?: ModelStringInput | null;
  investmentType?: ModelStringInput | null;
  description?: ModelStringInput | null;
  amount?: ModelFloatInput | null;
  status?: ModelStringInput | null;
  submittedByEmail?: ModelStringInput | null;
  organizationID?: ModelIDInput | null;
  zinstance?: ModelStringInput | null;
  agents?: ModelStringInput | null;
  and?: Array<ModelRequestFilterInput | null> | null;
  or?: Array<ModelRequestFilterInput | null> | null;
  not?: ModelRequestFilterInput | null;
};

export type ModelRequestConnection = {
  __typename: "ModelRequestConnection";
  items?: Array<Request | null> | null;
  nextToken?: string | null;
};

export type ModelApproverFilterInput = {
  id?: ModelIDInput | null;
  user?: ModelStringInput | null;
  organizationID?: ModelIDInput | null;
  workflowInstanceId?: ModelIDInput | null;
  and?: Array<ModelApproverFilterInput | null> | null;
  or?: Array<ModelApproverFilterInput | null> | null;
  not?: ModelApproverFilterInput | null;
};

export type ModelApproverConnection = {
  __typename: "ModelApproverConnection";
  items?: Array<Approver | null> | null;
  nextToken?: string | null;
};

export type ModelWorkflowInstanceFilterInput = {
  id?: ModelIDInput | null;
  organizationID?: ModelIDInput | null;
  workflowDefinitionId?: ModelIDInput | null;
  inProgress?: ModelStringInput | null;
  currentLevel?: ModelStringInput | null;
  currentStep?: ModelStringInput | null;
  and?: Array<ModelWorkflowInstanceFilterInput | null> | null;
  or?: Array<ModelWorkflowInstanceFilterInput | null> | null;
  not?: ModelWorkflowInstanceFilterInput | null;
};

export type ModelWorkflowInstanceConnection = {
  __typename: "ModelWorkflowInstanceConnection";
  items?: Array<WorkflowInstance | null> | null;
  nextToken?: string | null;
};

export type ReadRequestsToSendToSAPInput = {
  organizationID?: string | null;
};

export type ReadAttachmentInput = {
  requestId?: string | null;
  attachment?: AttachmentInput | null;
};

export type CreateSubscriberMutation = {
  __typename: "Subscriber";
  subdomain: string;
  organizationID?: string | null;
  user_pools_id?: string | null;
  app_client_id?: string | null;
  federated?: boolean | null;
  federated_domain?: string | null;
  redirectSignIn?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateSubscriberMutation = {
  __typename: "Subscriber";
  subdomain: string;
  organizationID?: string | null;
  user_pools_id?: string | null;
  app_client_id?: string | null;
  federated?: boolean | null;
  federated_domain?: string | null;
  redirectSignIn?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteSubscriberMutation = {
  __typename: "Subscriber";
  subdomain: string;
  organizationID?: string | null;
  user_pools_id?: string | null;
  app_client_id?: string | null;
  federated?: boolean | null;
  federated_domain?: string | null;
  redirectSignIn?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type CreateOrganizationMutation = {
  __typename: "Organization";
  id: string;
  name: string;
  companyCodes?: Array<{
    __typename: "CompanyCode";
    code?: string | null;
    description?: string | null;
  } | null> | null;
  workflowDefinition?: {
    __typename: "WorkflowDefinition";
    id?: string | null;
    name?: string | null;
    levels?: Array<{
      __typename: "WorkflowLevel";
      id?: string | null;
      name?: string | null;
    } | null> | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteOrganizationMutation = {
  __typename: "Organization";
  id: string;
  name: string;
  companyCodes?: Array<{
    __typename: "CompanyCode";
    code?: string | null;
    description?: string | null;
  } | null> | null;
  workflowDefinition?: {
    __typename: "WorkflowDefinition";
    id?: string | null;
    name?: string | null;
    levels?: Array<{
      __typename: "WorkflowLevel";
      id?: string | null;
      name?: string | null;
    } | null> | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateOrganizationMutation = {
  __typename: "Organization";
  id: string;
  name: string;
  companyCodes?: Array<{
    __typename: "CompanyCode";
    code?: string | null;
    description?: string | null;
  } | null> | null;
  workflowDefinition?: {
    __typename: "WorkflowDefinition";
    id?: string | null;
    name?: string | null;
    levels?: Array<{
      __typename: "WorkflowLevel";
      id?: string | null;
      name?: string | null;
    } | null> | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type CreateRequestMutation = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateRequestMutation = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteRequestMutation = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type CreateApproverMutation = {
  __typename: "Approver";
  id: string;
  user: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  createdAt: string;
  updatedAt: string;
  RequestToApprove?: {
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type UpdateApproverMutation = {
  __typename: "Approver";
  id: string;
  user: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  createdAt: string;
  updatedAt: string;
  RequestToApprove?: {
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type DeleteApproverMutation = {
  __typename: "Approver";
  id: string;
  user: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  createdAt: string;
  updatedAt: string;
  RequestToApprove?: {
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type CreateWorkflowInstanceMutation = {
  __typename: "WorkflowInstance";
  id: string;
  organizationID: string;
  workflowDefinitionId: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateWorkflowInstanceMutation = {
  __typename: "WorkflowInstance";
  id: string;
  organizationID: string;
  workflowDefinitionId: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteWorkflowInstanceMutation = {
  __typename: "WorkflowInstance";
  id: string;
  organizationID: string;
  workflowDefinitionId: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type SubmitRequestMutation = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type GetSubscriberQuery = {
  __typename: "Subscriber";
  subdomain: string;
  organizationID?: string | null;
  user_pools_id?: string | null;
  app_client_id?: string | null;
  federated?: boolean | null;
  federated_domain?: string | null;
  redirectSignIn?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type ListSubscribersQuery = {
  __typename: "ModelSubscriberConnection";
  items?: Array<{
    __typename: "Subscriber";
    subdomain: string;
    organizationID?: string | null;
    user_pools_id?: string | null;
    app_client_id?: string | null;
    federated?: boolean | null;
    federated_domain?: string | null;
    redirectSignIn?: string | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type SubscribersByUserPoolIdQuery = {
  __typename: "ModelSubscriberConnection";
  items?: Array<{
    __typename: "Subscriber";
    subdomain: string;
    organizationID?: string | null;
    user_pools_id?: string | null;
    app_client_id?: string | null;
    federated?: boolean | null;
    federated_domain?: string | null;
    redirectSignIn?: string | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type GetOrganizationQuery = {
  __typename: "Organization";
  id: string;
  name: string;
  companyCodes?: Array<{
    __typename: "CompanyCode";
    code?: string | null;
    description?: string | null;
  } | null> | null;
  workflowDefinition?: {
    __typename: "WorkflowDefinition";
    id?: string | null;
    name?: string | null;
    levels?: Array<{
      __typename: "WorkflowLevel";
      id?: string | null;
      name?: string | null;
    } | null> | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type ListOrganizationsQuery = {
  __typename: "ModelOrganizationConnection";
  items?: Array<{
    __typename: "Organization";
    id: string;
    name: string;
    companyCodes?: Array<{
      __typename: "CompanyCode";
      code?: string | null;
      description?: string | null;
    } | null> | null;
    workflowDefinition?: {
      __typename: "WorkflowDefinition";
      id?: string | null;
      name?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type ListRequestsQuery = {
  __typename: "ModelRequestConnection";
  items?: Array<{
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type GetRequestQuery = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type RequestsToSendToSapQuery = {
  __typename: "ModelRequestConnection";
  items?: Array<{
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type RequestsByOwnerQuery = {
  __typename: "ModelRequestConnection";
  items?: Array<{
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type GetApproverQuery = {
  __typename: "Approver";
  id: string;
  user: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  createdAt: string;
  updatedAt: string;
  RequestToApprove?: {
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type ListApproversQuery = {
  __typename: "ModelApproverConnection";
  items?: Array<{
    __typename: "Approver";
    id: string;
    user: string;
    organizationID?: string | null;
    workflowInstanceId?: string | null;
    createdAt: string;
    updatedAt: string;
    RequestToApprove?: {
      __typename: "Request";
      id: string;
      owner?: string | null;
      sendToSAP?: string | null;
      companyCode?: string | null;
      investmentType?: string | null;
      description?: string | null;
      amount?: number | null;
      status?: string | null;
      submittedByEmail?: string | null;
      organizationID: string;
      zinstance?: string | null;
      agents?: Array<string | null> | null;
      createdAt: string;
      updatedAt: string;
    } | null;
  } | null> | null;
  nextToken?: string | null;
};

export type ApprovalsByUserQuery = {
  __typename: "ModelApproverConnection";
  items?: Array<{
    __typename: "Approver";
    id: string;
    user: string;
    organizationID?: string | null;
    workflowInstanceId?: string | null;
    createdAt: string;
    updatedAt: string;
    RequestToApprove?: {
      __typename: "Request";
      id: string;
      owner?: string | null;
      sendToSAP?: string | null;
      companyCode?: string | null;
      investmentType?: string | null;
      description?: string | null;
      amount?: number | null;
      status?: string | null;
      submittedByEmail?: string | null;
      organizationID: string;
      zinstance?: string | null;
      agents?: Array<string | null> | null;
      createdAt: string;
      updatedAt: string;
    } | null;
  } | null> | null;
  nextToken?: string | null;
};

export type GetWorkflowInstanceQuery = {
  __typename: "WorkflowInstance";
  id: string;
  organizationID: string;
  workflowDefinitionId: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type ListWorkflowInstancesQuery = {
  __typename: "ModelWorkflowInstanceConnection";
  items?: Array<{
    __typename: "WorkflowInstance";
    id: string;
    organizationID: string;
    workflowDefinitionId: string;
    inProgress?: string | null;
    currentLevel?: string | null;
    currentStep?: string | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type ReadRequestsToSendToSapQuery = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type ReadAttachmentQuery = {
  __typename: "Attachment";
  fileName: string;
  fileSize?: number | null;
  fileType?: string | null;
  key: string;
  identity?: string | null;
  url?: string | null;
};

export type OnCreateOrganizationSubscription = {
  __typename: "Organization";
  id: string;
  name: string;
  companyCodes?: Array<{
    __typename: "CompanyCode";
    code?: string | null;
    description?: string | null;
  } | null> | null;
  workflowDefinition?: {
    __typename: "WorkflowDefinition";
    id?: string | null;
    name?: string | null;
    levels?: Array<{
      __typename: "WorkflowLevel";
      id?: string | null;
      name?: string | null;
    } | null> | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateOrganizationSubscription = {
  __typename: "Organization";
  id: string;
  name: string;
  companyCodes?: Array<{
    __typename: "CompanyCode";
    code?: string | null;
    description?: string | null;
  } | null> | null;
  workflowDefinition?: {
    __typename: "WorkflowDefinition";
    id?: string | null;
    name?: string | null;
    levels?: Array<{
      __typename: "WorkflowLevel";
      id?: string | null;
      name?: string | null;
    } | null> | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteOrganizationSubscription = {
  __typename: "Organization";
  id: string;
  name: string;
  companyCodes?: Array<{
    __typename: "CompanyCode";
    code?: string | null;
    description?: string | null;
  } | null> | null;
  workflowDefinition?: {
    __typename: "WorkflowDefinition";
    id?: string | null;
    name?: string | null;
    levels?: Array<{
      __typename: "WorkflowLevel";
      id?: string | null;
      name?: string | null;
    } | null> | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnCreateRequestSubscription = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateRequestSubscription = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteRequestSubscription = {
  __typename: "Request";
  id: string;
  owner?: string | null;
  sendToSAP?: string | null;
  companyCode?: string | null;
  investmentType?: string | null;
  description?: string | null;
  amount?: number | null;
  status?: string | null;
  submittedByEmail?: string | null;
  organizationID: string;
  zinstance?: string | null;
  attachments?: Array<{
    __typename: "Attachment";
    fileName: string;
    fileSize?: number | null;
    fileType?: string | null;
    key: string;
    identity?: string | null;
    url?: string | null;
  } | null> | null;
  agents?: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
};

export type OnCreateApproverSubscription = {
  __typename: "Approver";
  id: string;
  user: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  createdAt: string;
  updatedAt: string;
  RequestToApprove?: {
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type OnUpdateApproverSubscription = {
  __typename: "Approver";
  id: string;
  user: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  createdAt: string;
  updatedAt: string;
  RequestToApprove?: {
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type OnDeleteApproverSubscription = {
  __typename: "Approver";
  id: string;
  user: string;
  organizationID?: string | null;
  workflowInstanceId?: string | null;
  createdAt: string;
  updatedAt: string;
  RequestToApprove?: {
    __typename: "Request";
    id: string;
    owner?: string | null;
    sendToSAP?: string | null;
    companyCode?: string | null;
    investmentType?: string | null;
    description?: string | null;
    amount?: number | null;
    status?: string | null;
    submittedByEmail?: string | null;
    organizationID: string;
    zinstance?: string | null;
    attachments?: Array<{
      __typename: "Attachment";
      fileName: string;
      fileSize?: number | null;
      fileType?: string | null;
      key: string;
      identity?: string | null;
      url?: string | null;
    } | null> | null;
    agents?: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
  } | null;
};

export type OnCreateWorkflowInstanceSubscription = {
  __typename: "WorkflowInstance";
  id: string;
  organizationID: string;
  workflowDefinitionId: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateWorkflowInstanceSubscription = {
  __typename: "WorkflowInstance";
  id: string;
  organizationID: string;
  workflowDefinitionId: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteWorkflowInstanceSubscription = {
  __typename: "WorkflowInstance";
  id: string;
  organizationID: string;
  workflowDefinitionId: string;
  inProgress?: string | null;
  currentLevel?: string | null;
  currentStep?: string | null;
  createdAt: string;
  updatedAt: string;
};

@Injectable({
  providedIn: "root"
})
export class APIService {
  async CreateSubscriber(
    input: CreateSubscriberInput,
    condition?: ModelSubscriberConditionInput
  ): Promise<CreateSubscriberMutation> {
    const statement = `mutation CreateSubscriber($input: CreateSubscriberInput!, $condition: ModelSubscriberConditionInput) {
        createSubscriber(input: $input, condition: $condition) {
          __typename
          subdomain
          organizationID
          user_pools_id
          app_client_id
          federated
          federated_domain
          redirectSignIn
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateSubscriberMutation>response.data.createSubscriber;
  }
  async UpdateSubscriber(
    input: UpdateSubscriberInput,
    condition?: ModelSubscriberConditionInput
  ): Promise<UpdateSubscriberMutation> {
    const statement = `mutation UpdateSubscriber($input: UpdateSubscriberInput!, $condition: ModelSubscriberConditionInput) {
        updateSubscriber(input: $input, condition: $condition) {
          __typename
          subdomain
          organizationID
          user_pools_id
          app_client_id
          federated
          federated_domain
          redirectSignIn
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateSubscriberMutation>response.data.updateSubscriber;
  }
  async DeleteSubscriber(
    input: DeleteSubscriberInput,
    condition?: ModelSubscriberConditionInput
  ): Promise<DeleteSubscriberMutation> {
    const statement = `mutation DeleteSubscriber($input: DeleteSubscriberInput!, $condition: ModelSubscriberConditionInput) {
        deleteSubscriber(input: $input, condition: $condition) {
          __typename
          subdomain
          organizationID
          user_pools_id
          app_client_id
          federated
          federated_domain
          redirectSignIn
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteSubscriberMutation>response.data.deleteSubscriber;
  }
  async CreateOrganization(
    input: CreateOrganizationInput,
    condition?: ModelOrganizationConditionInput
  ): Promise<CreateOrganizationMutation> {
    const statement = `mutation CreateOrganization($input: CreateOrganizationInput!, $condition: ModelOrganizationConditionInput) {
        createOrganization(input: $input, condition: $condition) {
          __typename
          id
          name
          companyCodes {
            __typename
            code
            description
          }
          workflowDefinition {
            __typename
            id
            name
            levels {
              __typename
              id
              name
            }
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateOrganizationMutation>response.data.createOrganization;
  }
  async DeleteOrganization(
    input: DeleteOrganizationInput,
    condition?: ModelOrganizationConditionInput
  ): Promise<DeleteOrganizationMutation> {
    const statement = `mutation DeleteOrganization($input: DeleteOrganizationInput!, $condition: ModelOrganizationConditionInput) {
        deleteOrganization(input: $input, condition: $condition) {
          __typename
          id
          name
          companyCodes {
            __typename
            code
            description
          }
          workflowDefinition {
            __typename
            id
            name
            levels {
              __typename
              id
              name
            }
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteOrganizationMutation>response.data.deleteOrganization;
  }
  async UpdateOrganization(
    input: UpdateOrganizationInput,
    condition?: ModelOrganizationConditionInput
  ): Promise<UpdateOrganizationMutation> {
    const statement = `mutation UpdateOrganization($input: UpdateOrganizationInput!, $condition: ModelOrganizationConditionInput) {
        updateOrganization(input: $input, condition: $condition) {
          __typename
          id
          name
          companyCodes {
            __typename
            code
            description
          }
          workflowDefinition {
            __typename
            id
            name
            levels {
              __typename
              id
              name
            }
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateOrganizationMutation>response.data.updateOrganization;
  }
  async CreateRequest(
    input: CreateRequestInput,
    condition?: ModelRequestConditionInput
  ): Promise<CreateRequestMutation> {
    const statement = `mutation CreateRequest($input: CreateRequestInput!, $condition: ModelRequestConditionInput) {
        createRequest(input: $input, condition: $condition) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateRequestMutation>response.data.createRequest;
  }
  async UpdateRequest(
    input: UpdateRequestInput,
    condition?: ModelRequestConditionInput
  ): Promise<UpdateRequestMutation> {
    const statement = `mutation UpdateRequest($input: UpdateRequestInput!, $condition: ModelRequestConditionInput) {
        updateRequest(input: $input, condition: $condition) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateRequestMutation>response.data.updateRequest;
  }
  async DeleteRequest(
    input: DeleteRequestInput,
    condition?: ModelRequestConditionInput
  ): Promise<DeleteRequestMutation> {
    const statement = `mutation DeleteRequest($input: DeleteRequestInput!, $condition: ModelRequestConditionInput) {
        deleteRequest(input: $input, condition: $condition) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteRequestMutation>response.data.deleteRequest;
  }
  async CreateApprover(
    input: CreateApproverInput,
    condition?: ModelApproverConditionInput
  ): Promise<CreateApproverMutation> {
    const statement = `mutation CreateApprover($input: CreateApproverInput!, $condition: ModelApproverConditionInput) {
        createApprover(input: $input, condition: $condition) {
          __typename
          id
          user
          organizationID
          workflowInstanceId
          createdAt
          updatedAt
          RequestToApprove {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateApproverMutation>response.data.createApprover;
  }
  async UpdateApprover(
    input: UpdateApproverInput,
    condition?: ModelApproverConditionInput
  ): Promise<UpdateApproverMutation> {
    const statement = `mutation UpdateApprover($input: UpdateApproverInput!, $condition: ModelApproverConditionInput) {
        updateApprover(input: $input, condition: $condition) {
          __typename
          id
          user
          organizationID
          workflowInstanceId
          createdAt
          updatedAt
          RequestToApprove {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateApproverMutation>response.data.updateApprover;
  }
  async DeleteApprover(
    input: DeleteApproverInput,
    condition?: ModelApproverConditionInput
  ): Promise<DeleteApproverMutation> {
    const statement = `mutation DeleteApprover($input: DeleteApproverInput!, $condition: ModelApproverConditionInput) {
        deleteApprover(input: $input, condition: $condition) {
          __typename
          id
          user
          organizationID
          workflowInstanceId
          createdAt
          updatedAt
          RequestToApprove {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteApproverMutation>response.data.deleteApprover;
  }
  async CreateWorkflowInstance(
    input: CreateWorkflowInstanceInput,
    condition?: ModelWorkflowInstanceConditionInput
  ): Promise<CreateWorkflowInstanceMutation> {
    const statement = `mutation CreateWorkflowInstance($input: CreateWorkflowInstanceInput!, $condition: ModelWorkflowInstanceConditionInput) {
        createWorkflowInstance(input: $input, condition: $condition) {
          __typename
          id
          organizationID
          workflowDefinitionId
          inProgress
          currentLevel
          currentStep
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateWorkflowInstanceMutation>response.data.createWorkflowInstance;
  }
  async UpdateWorkflowInstance(
    input: UpdateWorkflowInstanceInput,
    condition?: ModelWorkflowInstanceConditionInput
  ): Promise<UpdateWorkflowInstanceMutation> {
    const statement = `mutation UpdateWorkflowInstance($input: UpdateWorkflowInstanceInput!, $condition: ModelWorkflowInstanceConditionInput) {
        updateWorkflowInstance(input: $input, condition: $condition) {
          __typename
          id
          organizationID
          workflowDefinitionId
          inProgress
          currentLevel
          currentStep
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateWorkflowInstanceMutation>response.data.updateWorkflowInstance;
  }
  async DeleteWorkflowInstance(
    input: DeleteWorkflowInstanceInput,
    condition?: ModelWorkflowInstanceConditionInput
  ): Promise<DeleteWorkflowInstanceMutation> {
    const statement = `mutation DeleteWorkflowInstance($input: DeleteWorkflowInstanceInput!, $condition: ModelWorkflowInstanceConditionInput) {
        deleteWorkflowInstance(input: $input, condition: $condition) {
          __typename
          id
          organizationID
          workflowDefinitionId
          inProgress
          currentLevel
          currentStep
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteWorkflowInstanceMutation>response.data.deleteWorkflowInstance;
  }
  async SubmitRequest(
    header: SubmitRequestInput
  ): Promise<SubmitRequestMutation> {
    const statement = `mutation SubmitRequest($header: SubmitRequestInput!) {
        submitRequest(header: $header) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      header
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <SubmitRequestMutation>response.data.submitRequest;
  }
  async GetSubscriber(subdomain: string): Promise<GetSubscriberQuery> {
    const statement = `query GetSubscriber($subdomain: String!) {
        getSubscriber(subdomain: $subdomain) {
          __typename
          subdomain
          organizationID
          user_pools_id
          app_client_id
          federated
          federated_domain
          redirectSignIn
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      subdomain
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetSubscriberQuery>response.data.getSubscriber;
  }
  async ListSubscribers(
    subdomain?: string,
    filter?: ModelSubscriberFilterInput,
    limit?: number,
    nextToken?: string,
    sortDirection?: ModelSortDirection
  ): Promise<ListSubscribersQuery> {
    const statement = `query ListSubscribers($subdomain: String, $filter: ModelSubscriberFilterInput, $limit: Int, $nextToken: String, $sortDirection: ModelSortDirection) {
        listSubscribers(subdomain: $subdomain, filter: $filter, limit: $limit, nextToken: $nextToken, sortDirection: $sortDirection) {
          __typename
          items {
            __typename
            subdomain
            organizationID
            user_pools_id
            app_client_id
            federated
            federated_domain
            redirectSignIn
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (subdomain) {
      gqlAPIServiceArguments.subdomain = subdomain;
    }
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    if (sortDirection) {
      gqlAPIServiceArguments.sortDirection = sortDirection;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListSubscribersQuery>response.data.listSubscribers;
  }
  async SubscribersByUserPoolId(
    user_pools_id?: string,
    sortDirection?: ModelSortDirection,
    filter?: ModelSubscriberFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<SubscribersByUserPoolIdQuery> {
    const statement = `query SubscribersByUserPoolId($user_pools_id: String, $sortDirection: ModelSortDirection, $filter: ModelSubscriberFilterInput, $limit: Int, $nextToken: String) {
        subscribersByUserPoolId(user_pools_id: $user_pools_id, sortDirection: $sortDirection, filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            subdomain
            organizationID
            user_pools_id
            app_client_id
            federated
            federated_domain
            redirectSignIn
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (user_pools_id) {
      gqlAPIServiceArguments.user_pools_id = user_pools_id;
    }
    if (sortDirection) {
      gqlAPIServiceArguments.sortDirection = sortDirection;
    }
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <SubscribersByUserPoolIdQuery>response.data.subscribersByUserPoolId;
  }
  async GetOrganization(id: string): Promise<GetOrganizationQuery> {
    const statement = `query GetOrganization($id: ID!) {
        getOrganization(id: $id) {
          __typename
          id
          name
          companyCodes {
            __typename
            code
            description
          }
          workflowDefinition {
            __typename
            id
            name
            levels {
              __typename
              id
              name
            }
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetOrganizationQuery>response.data.getOrganization;
  }
  async ListOrganizations(
    filter?: ModelOrganizationFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListOrganizationsQuery> {
    const statement = `query ListOrganizations($filter: ModelOrganizationFilterInput, $limit: Int, $nextToken: String) {
        listOrganizations(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            name
            companyCodes {
              __typename
              code
              description
            }
            workflowDefinition {
              __typename
              id
              name
            }
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListOrganizationsQuery>response.data.listOrganizations;
  }
  async ListRequests(
    id?: string,
    filter?: ModelRequestFilterInput,
    limit?: number,
    nextToken?: string,
    sortDirection?: ModelSortDirection
  ): Promise<ListRequestsQuery> {
    const statement = `query ListRequests($id: ID, $filter: ModelRequestFilterInput, $limit: Int, $nextToken: String, $sortDirection: ModelSortDirection) {
        listRequests(id: $id, filter: $filter, limit: $limit, nextToken: $nextToken, sortDirection: $sortDirection) {
          __typename
          items {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (id) {
      gqlAPIServiceArguments.id = id;
    }
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    if (sortDirection) {
      gqlAPIServiceArguments.sortDirection = sortDirection;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListRequestsQuery>response.data.listRequests;
  }
  async GetRequest(id: string): Promise<GetRequestQuery> {
    const statement = `query GetRequest($id: ID!) {
        getRequest(id: $id) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetRequestQuery>response.data.getRequest;
  }
  async RequestsToSendToSap(
    sendToSAP?: string,
    sortDirection?: ModelSortDirection,
    filter?: ModelRequestFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<RequestsToSendToSapQuery> {
    const statement = `query RequestsToSendToSap($sendToSAP: String, $sortDirection: ModelSortDirection, $filter: ModelRequestFilterInput, $limit: Int, $nextToken: String) {
        requestsToSendToSAP(sendToSAP: $sendToSAP, sortDirection: $sortDirection, filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (sendToSAP) {
      gqlAPIServiceArguments.sendToSAP = sendToSAP;
    }
    if (sortDirection) {
      gqlAPIServiceArguments.sortDirection = sortDirection;
    }
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <RequestsToSendToSapQuery>response.data.requestsToSendToSAP;
  }
  async RequestsByOwner(
    owner?: string,
    sortDirection?: ModelSortDirection,
    filter?: ModelRequestFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<RequestsByOwnerQuery> {
    const statement = `query RequestsByOwner($owner: String, $sortDirection: ModelSortDirection, $filter: ModelRequestFilterInput, $limit: Int, $nextToken: String) {
        requestsByOwner(owner: $owner, sortDirection: $sortDirection, filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    if (sortDirection) {
      gqlAPIServiceArguments.sortDirection = sortDirection;
    }
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <RequestsByOwnerQuery>response.data.requestsByOwner;
  }
  async GetApprover(id: string): Promise<GetApproverQuery> {
    const statement = `query GetApprover($id: ID!) {
        getApprover(id: $id) {
          __typename
          id
          user
          organizationID
          workflowInstanceId
          createdAt
          updatedAt
          RequestToApprove {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetApproverQuery>response.data.getApprover;
  }
  async ListApprovers(
    filter?: ModelApproverFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListApproversQuery> {
    const statement = `query ListApprovers($filter: ModelApproverFilterInput, $limit: Int, $nextToken: String) {
        listApprovers(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            user
            organizationID
            workflowInstanceId
            createdAt
            updatedAt
            RequestToApprove {
              __typename
              id
              owner
              sendToSAP
              companyCode
              investmentType
              description
              amount
              status
              submittedByEmail
              organizationID
              zinstance
              agents
              createdAt
              updatedAt
            }
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListApproversQuery>response.data.listApprovers;
  }
  async ApprovalsByUser(
    user?: string,
    sortDirection?: ModelSortDirection,
    filter?: ModelApproverFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ApprovalsByUserQuery> {
    const statement = `query ApprovalsByUser($user: String, $sortDirection: ModelSortDirection, $filter: ModelApproverFilterInput, $limit: Int, $nextToken: String) {
        approvalsByUser(user: $user, sortDirection: $sortDirection, filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            user
            organizationID
            workflowInstanceId
            createdAt
            updatedAt
            RequestToApprove {
              __typename
              id
              owner
              sendToSAP
              companyCode
              investmentType
              description
              amount
              status
              submittedByEmail
              organizationID
              zinstance
              agents
              createdAt
              updatedAt
            }
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (user) {
      gqlAPIServiceArguments.user = user;
    }
    if (sortDirection) {
      gqlAPIServiceArguments.sortDirection = sortDirection;
    }
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ApprovalsByUserQuery>response.data.approvalsByUser;
  }
  async GetWorkflowInstance(id: string): Promise<GetWorkflowInstanceQuery> {
    const statement = `query GetWorkflowInstance($id: ID!) {
        getWorkflowInstance(id: $id) {
          __typename
          id
          organizationID
          workflowDefinitionId
          inProgress
          currentLevel
          currentStep
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetWorkflowInstanceQuery>response.data.getWorkflowInstance;
  }
  async ListWorkflowInstances(
    filter?: ModelWorkflowInstanceFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListWorkflowInstancesQuery> {
    const statement = `query ListWorkflowInstances($filter: ModelWorkflowInstanceFilterInput, $limit: Int, $nextToken: String) {
        listWorkflowInstances(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            organizationID
            workflowDefinitionId
            inProgress
            currentLevel
            currentStep
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListWorkflowInstancesQuery>response.data.listWorkflowInstances;
  }
  async ReadRequestsToSendToSap(
    input: ReadRequestsToSendToSAPInput
  ): Promise<Array<ReadRequestsToSendToSapQuery>> {
    const statement = `query ReadRequestsToSendToSap($input: ReadRequestsToSendToSAPInput!) {
        readRequestsToSendToSAP(input: $input) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <Array<ReadRequestsToSendToSapQuery>>(
      response.data.readRequestsToSendToSAP
    );
  }
  async ReadAttachment(
    input: ReadAttachmentInput
  ): Promise<ReadAttachmentQuery> {
    const statement = `query ReadAttachment($input: ReadAttachmentInput!) {
        readAttachment(input: $input) {
          __typename
          fileName
          fileSize
          fileType
          key
          identity
          url
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ReadAttachmentQuery>response.data.readAttachment;
  }
  OnCreateOrganizationListener: Observable<
    SubscriptionResponse<OnCreateOrganizationSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateOrganization {
        onCreateOrganization {
          __typename
          id
          name
          companyCodes {
            __typename
            code
            description
          }
          workflowDefinition {
            __typename
            id
            name
            levels {
              __typename
              id
              name
            }
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnCreateOrganizationSubscription>>;

  OnUpdateOrganizationListener: Observable<
    SubscriptionResponse<OnUpdateOrganizationSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateOrganization {
        onUpdateOrganization {
          __typename
          id
          name
          companyCodes {
            __typename
            code
            description
          }
          workflowDefinition {
            __typename
            id
            name
            levels {
              __typename
              id
              name
            }
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnUpdateOrganizationSubscription>>;

  OnDeleteOrganizationListener: Observable<
    SubscriptionResponse<OnDeleteOrganizationSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteOrganization {
        onDeleteOrganization {
          __typename
          id
          name
          companyCodes {
            __typename
            code
            description
          }
          workflowDefinition {
            __typename
            id
            name
            levels {
              __typename
              id
              name
            }
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnDeleteOrganizationSubscription>>;

  OnCreateRequestListener(
    owner?: string,
    agents?: string
  ): Observable<SubscriptionResponse<OnCreateRequestSubscription>> {
    const statement = `subscription OnCreateRequest($owner: String, $agents: String) {
        onCreateRequest(owner: $owner, agents: $agents) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    if (agents) {
      gqlAPIServiceArguments.agents = agents;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<SubscriptionResponse<OnCreateRequestSubscription>>;
  }

  OnUpdateRequestListener(
    owner?: string,
    agents?: string
  ): Observable<SubscriptionResponse<OnUpdateRequestSubscription>> {
    const statement = `subscription OnUpdateRequest($owner: String, $agents: String) {
        onUpdateRequest(owner: $owner, agents: $agents) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    if (agents) {
      gqlAPIServiceArguments.agents = agents;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<SubscriptionResponse<OnUpdateRequestSubscription>>;
  }

  OnDeleteRequestListener(
    owner?: string,
    agents?: string
  ): Observable<SubscriptionResponse<OnDeleteRequestSubscription>> {
    const statement = `subscription OnDeleteRequest($owner: String, $agents: String) {
        onDeleteRequest(owner: $owner, agents: $agents) {
          __typename
          id
          owner
          sendToSAP
          companyCode
          investmentType
          description
          amount
          status
          submittedByEmail
          organizationID
          zinstance
          attachments {
            __typename
            fileName
            fileSize
            fileType
            key
            identity
            url
          }
          agents
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    if (agents) {
      gqlAPIServiceArguments.agents = agents;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<SubscriptionResponse<OnDeleteRequestSubscription>>;
  }

  OnCreateApproverListener(
    user?: string
  ): Observable<SubscriptionResponse<OnCreateApproverSubscription>> {
    const statement = `subscription OnCreateApprover($user: String) {
        onCreateApprover(user: $user) {
          __typename
          id
          user
          organizationID
          workflowInstanceId
          createdAt
          updatedAt
          RequestToApprove {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (user) {
      gqlAPIServiceArguments.user = user;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<SubscriptionResponse<OnCreateApproverSubscription>>;
  }

  OnUpdateApproverListener(
    user?: string
  ): Observable<SubscriptionResponse<OnUpdateApproverSubscription>> {
    const statement = `subscription OnUpdateApprover($user: String) {
        onUpdateApprover(user: $user) {
          __typename
          id
          user
          organizationID
          workflowInstanceId
          createdAt
          updatedAt
          RequestToApprove {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (user) {
      gqlAPIServiceArguments.user = user;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<SubscriptionResponse<OnUpdateApproverSubscription>>;
  }

  OnDeleteApproverListener(
    user?: string
  ): Observable<SubscriptionResponse<OnDeleteApproverSubscription>> {
    const statement = `subscription OnDeleteApprover($user: String) {
        onDeleteApprover(user: $user) {
          __typename
          id
          user
          organizationID
          workflowInstanceId
          createdAt
          updatedAt
          RequestToApprove {
            __typename
            id
            owner
            sendToSAP
            companyCode
            investmentType
            description
            amount
            status
            submittedByEmail
            organizationID
            zinstance
            attachments {
              __typename
              fileName
              fileSize
              fileType
              key
              identity
              url
            }
            agents
            createdAt
            updatedAt
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (user) {
      gqlAPIServiceArguments.user = user;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<SubscriptionResponse<OnDeleteApproverSubscription>>;
  }

  OnCreateWorkflowInstanceListener: Observable<
    SubscriptionResponse<OnCreateWorkflowInstanceSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateWorkflowInstance {
        onCreateWorkflowInstance {
          __typename
          id
          organizationID
          workflowDefinitionId
          inProgress
          currentLevel
          currentStep
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnCreateWorkflowInstanceSubscription>>;

  OnUpdateWorkflowInstanceListener: Observable<
    SubscriptionResponse<OnUpdateWorkflowInstanceSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateWorkflowInstance {
        onUpdateWorkflowInstance {
          __typename
          id
          organizationID
          workflowDefinitionId
          inProgress
          currentLevel
          currentStep
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnUpdateWorkflowInstanceSubscription>>;

  OnDeleteWorkflowInstanceListener: Observable<
    SubscriptionResponse<OnDeleteWorkflowInstanceSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteWorkflowInstance {
        onDeleteWorkflowInstance {
          __typename
          id
          organizationID
          workflowDefinitionId
          inProgress
          currentLevel
          currentStep
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnDeleteWorkflowInstanceSubscription>>;
}
