import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Auth } from 'aws-amplify'; //TO DO - change to Auth Context

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private router: Router){

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //return true;
    return new Promise((resolve) => {
      Auth.currentAuthenticatedUser({
          bypassCache: false
        })
        .then((user) => {
          if(user){
            console.log("ok")
            if(user.signInUserSession.accessToken.payload["cognito:groups"]){
              if(user.signInUserSession.accessToken.payload["cognito:groups"].find((g:String)=>{
                return g === "Admin"
              })){
                resolve(true);
              }
            }else{
              resolve(false)
            }
            
          }
        })
        .catch(() => {
          this.router.navigate(['/']);
          resolve(false);
        });
    });
  }
  
}
