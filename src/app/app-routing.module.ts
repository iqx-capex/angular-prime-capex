import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminGuard } from './admin.guard'

import { OrganizationComponent } from './organization/organization.component';
import { RequestComponent } from './request/request.component';
import { MyRequestsComponent } from './my-requests/my-requests.component';
import { MyApprovalsComponent } from './my-approvals/my-approvals.component';


const routes: Routes = [
  {path:"", component:RequestComponent},
  {path:"config", 
    component:OrganizationComponent,
    canActivate: [AdminGuard]
  },
  {path:"request", component:RequestComponent},
  {path:"request/:id", component:RequestComponent},
  {path:"myrequests", component:MyRequestsComponent},
  {path:"myapprovals", component:MyApprovalsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
