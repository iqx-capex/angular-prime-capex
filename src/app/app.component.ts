import { Component, OnInit, AfterViewInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { onAuthUIStateChange, CognitoUserInterface, AuthState, FormFieldTypes } from '@aws-amplify/ui-components';
import { Auth, Hub } from "aws-amplify";
import { Router } from '@angular/router';
import { Menu } from 'primeng/menu';
import { MenuItem } from 'primeng/api';
import { AuthService, AuthControl, initialAuthControl } from './services/auth.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('menu') menu!:Menu;

  user: CognitoUserInterface | undefined;
  authControl: AuthControl = initialAuthControl
  authState!: AuthState
  // formFields: FormFieldTypes;
  items!: MenuItem[]
  title = 'IQX Capex Request'
  email = ''
  // subscriber: string = ''
  // loading: boolean = true
  // federated: boolean = false
  // hosted_ui_url: string = ''

  constructor(public authService: AuthService,
             private router: Router, 
             private ref: ChangeDetectorRef) {


  }
  ngAfterViewInit(){
    this.menu.hide()
  }
  ngOnInit(): void {

    this.authService.AuthContext$.subscribe(event => {
      this.authControl = event
      this.ref.detectChanges();

    })

    onAuthUIStateChange((authState, authData) => {
  
      console.log("auth state change")
      console.log(authState)
      this.authState = authState;
      this.user = authData as CognitoUserInterface;
      //console.log(this.user)
      if (authState === 'signedin') {
        this.authService.refresh()
        this.email = this.user.attributes.email

        if(this.authControl.admin){
          console.log("admin")
          this.items.push(
            {
              label: 'Configuration', icon: 'pi pi-fw pi-cog', command: () => {
                this.router.navigate(['/config'])
              }
            }
          )
        }
        this.menu.show(true)
      } else {
        this.email = ''
      }
      this.ref.detectChanges();
      
    })

 
    Auth.currentAuthenticatedUser().then(user => {    //repeated code!
      if (user) {
        //       console.log(user)
        Auth.currentSession().then((attributes) => {
          this.email = attributes.getIdToken().payload.email;
        })
        // this.email = user.attributes.email
      }

    }).catch((error) => {
      console.log(error)
    })

    this.items = [
      {
        label: 'New Request', icon: 'pi pi-fw pi-plus', command: () => {
          this.router.navigate(['/request'])
        }
      },
      {
        label: 'My Requests', icon: 'pi pi-fw pi-list', command: () => {
          this.router.navigate(['/myrequests'])
        }
      },
      {
        label: 'My Approvals', icon: 'pi pi-fw pi-check-circle', command: () => {
          this.router.navigate(['/myapprovals'])
        }
      }
      // {
      //   label: 'Configuration', icon: 'pi pi-fw pi-cog', command: () => {
      //     this.router.navigate(['/config'])
      //   }
      // }
    ];
    
  }

  onFederatedSignIn(){
    window.location.assign(this.authControl.hosted_ui_url)
  //  Auth.federatedSignIn().then((cred)=>{
  //    console.log("cred", cred)
  //  }).catch((error)=>{
  //    console.log("error", error)
  //  })
  }

  ngOnDestroy() {
    return onAuthUIStateChange;
  }

}


