import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AmplifyUIAngularModule } from '@aws-amplify/ui-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {ButtonModule} from 'primeng/button';
import {DividerModule} from 'primeng/divider';
import {PanelModule} from 'primeng/panel';
import {TabViewModule} from 'primeng/tabview';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ToolbarModule} from 'primeng/toolbar';
import {MenubarModule} from 'primeng/menubar';
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import {ProgressBarModule} from 'primeng/progressbar';
import {DropdownModule} from 'primeng/dropdown';
import {MessageService} from 'primeng/api';
import {MessagesModule} from 'primeng/messages';
import {TooltipModule} from 'primeng/tooltip';
import {DataViewModule} from 'primeng/dataview';
import {MenuModule} from 'primeng/menu';
import {TagModule} from 'primeng/tag';
import {BlockUIModule} from 'primeng/blockui';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {FileUploadModule} from 'primeng/fileupload';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrganizationComponent } from './organization/organization.component';
import { RequestComponent } from './request/request.component';
import { MyRequestsComponent } from './my-requests/my-requests.component';
import { MyApprovalsComponent } from './my-approvals/my-approvals.component';


@NgModule({
  declarations: [
    AppComponent,
    OrganizationComponent,
    RequestComponent,
    MyRequestsComponent,
    MyApprovalsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AmplifyUIAngularModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    DividerModule,
    PanelModule,
    TabViewModule,
    MenubarModule,
    TooltipModule,
    InputTextModule,
    InputTextareaModule,
    ToolbarModule,
    TableModule,
    DropdownModule,
    ToastModule,
    ProgressBarModule,
    DataViewModule,
    MenuModule,
    TagModule,
    BlockUIModule,
    ScrollPanelModule,
    MessagesModule,
    FileUploadModule,
    HttpClientModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
