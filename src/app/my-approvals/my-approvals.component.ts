import { Component, OnInit } from '@angular/core';
import { MyapprovalsService } from '../services/myapprovals.service';

@Component({
  selector: 'app-my-approvals',
  templateUrl: './my-approvals.component.html',
  styleUrls: ['./my-approvals.component.css']
})
export class MyApprovalsComponent implements OnInit {

  constructor(public myApprovalsService:MyapprovalsService) {

  }

  ngOnInit(): void {
  }

}
