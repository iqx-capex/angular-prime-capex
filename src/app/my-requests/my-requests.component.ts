import { Component, OnInit } from '@angular/core';
import { MyrequestsService } from '../services/myrequests.service';

@Component({
  selector: 'app-my-requests',
  templateUrl: './my-requests.component.html',
  styleUrls: ['./my-requests.component.css']
})
export class MyRequestsComponent implements OnInit {

  constructor(public myRequestsService:MyrequestsService) {

  }

  ngOnInit(): void {
  }

}
