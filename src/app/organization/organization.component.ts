import { ChangeDetectionStrategy, Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { Subscription } from 'rxjs';
import { OrganizationService, Organization, CompanyCode } from '../services/organization.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class OrganizationComponent implements OnInit, OnDestroy {

  //@ViewChild('companyCodesTable') companyCodesTable!: TableModule;

  private subscriptions: Subscription[] = [];
  organization!: Organization
  companyCodes!: CompanyCode[]
  form: FormGroup
  blocked: boolean = false

  constructor(public organizationStore: OrganizationService, private messageService: MessageService) {

    this.form = new FormGroup({
      id: new FormControl(''),
      name: new FormControl('')
    });

  }

  ngOnInit(): void {

    this.blocked = false;
    this.subscriptions.push(this.organizationStore.organization$.subscribe(event => {
      this.organization = event
      this.companyCodes = this.organization.companyCodes as CompanyCode[]
      this.form.patchValue({
        id: this.organization.id,
        name: this.organization.name
      })

    })
    )

  }

  addCompany(): void {
    let companyCode: CompanyCode = { code: "", description: "" }
    this.companyCodes.push(companyCode)
  }

  removeCompany(companyCode: any): void {
    let i = this.companyCodes.findIndex((cc) => {
      return cc.code === companyCode.code
    })
    this.companyCodes.splice(i, 1)
  }

  saveOrganization(): void {
    this.blocked = true;
    this.organizationStore.updateOrganizationHeader(this.form.value)
    this.organizationStore.updateCompanyCodes(this.companyCodes)
    this.organizationStore.saveOrganization().then(() => {
      this.blocked = false;
      this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Organization updated', key:'organization' });
    }).catch(error=>{
      this.messageService.add({severity:'error', summary:'Error', detail:'Error updating Organization', key:'organization'});
      this.blocked = false;
    })

  }

  ngOnDestroy(): void {
    this.blocked = false;
    this.subscriptions.forEach(Subscription => Subscription.unsubscribe())
  }

}
