import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OrganizationService, CompanyCode } from '../services/organization.service';
import { APIService, CreateRequestInput, AttachmentInput } from '../API.service';  //temp - move into dedicated service
import { MessageService } from 'primeng/api';
import { Storage, Auth } from 'aws-amplify';    //move Auth to auth context / service
import { v4 as uuid } from 'uuid';


@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})


export class RequestComponent implements OnInit, OnDestroy {

  request: CreateRequestInput
  companyCodes: CompanyCode[] = []
  attachments: AttachmentInput[] = []
  form: FormGroup
  blocked: boolean = false
  locked: boolean = true
  uploading: boolean = false
  uploadProgress = 0
  private subscriptions: Subscription[] = []

  constructor(public organizationStore: OrganizationService,
    private api: APIService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute) {

    this.request = {
      id: "",
      companyCode: "1000",   //TO DO - set to first
      investmentType: "",
      description: "",
      amount: 0,
      organizationID: "",
      attachments: []

    }

    this.form = new FormGroup({
      companyCode: new FormControl(this.request.companyCode, Validators.required),
      description: new FormControl(this.request.description, Validators.required),
      amount: new FormControl(this.request.amount),
    });

    let that = this;
    this.subscriptions.push(this.organizationStore.companyCodes$.subscribe(event => {
      console.log("receive company codes")
      that.companyCodes = <CompanyCode[]>event

      this.setCompanyCodeDropDown()

    }))


  }

  ngOnInit(): void {

    this.blocked = false;

    const valueObj = this.organizationStore.companyCodes.find(c => c.code === this.request.companyCode);
    (this.form.get("companyCode") as any).setValue(valueObj)

    this.route.paramMap.subscribe(params => {

      console.log(params.get('id'))
      if (params.get('id') === null) {

        this.request = {
          id: "",
          companyCode: "1000",   //TO DO - set to first
          investmentType: "",
          description: "",
          amount: 0,
          organizationID: "",
          attachments: []

        }
        this.locked = false;

        this.form.setValue({      //repeated code!
          description: this.request.description,
          companyCode: this.request.companyCode,
          amount: this.request.amount
        })
        this.setCompanyCodeDropDown()

      } else {
        this.api.GetRequest(params.get('id') as string).then((r) => {
          Object.assign(this.request, r)

          this.form.setValue({
            description: this.request.description,
            companyCode: this.request.companyCode,
            amount: this.request.amount
          })
          this.setCompanyCodeDropDown()
          this.attachments = []
          if (this.request.attachments !== null && this.request.attachments !== undefined) {
            for (let file of this.request.attachments) {
              this.attachments.push(file as AttachmentInput)
            }
          }
          this.locked = true;
        })

      }

    });
  }

  setCompanyCodeDropDown() {
    if (this.companyCodes) {
      let companyCodeObj = this.companyCodes.find(c => c.code === this.request.companyCode);
      (this.form.get("companyCode") as any).setValue(companyCodeObj)
    }

  }
  updateCompanyCode() {
    console.log("updateCompanyCode")
    this.request.companyCode = this.form.value.companyCode.code
  }

  async onUpload(event: any) {

    const { identityId } = await Auth.currentCredentials(); //move to auth service
    for (let file of event.files) {
      let key = this.organizationStore.organization.id + "/" + uuid() + "_" + file.name
      this.uploading = true
      this.uploadProgress = 0
      let that = this;
      try {
        const result = await Storage.put(
          key,
          file,
          {
            level: 'protected',
            //  customPrefix: { protected: 'requests/' },
            progressCallback(progress: any) {
              that.uploadProgress = (progress.loaded / progress.total) * 100
            }
            //  metadata: { albumid: this.props.albumId }
          }
        );
        //console.log('Uploaded file: ', result);
        this.uploading = false
        let attachment: AttachmentInput = {
          fileName: file.name,
          fileSize: file.size,
          fileType: file.type,
          key: key,
          identity: identityId
        }
        this.request.attachments?.push(attachment)
        this.attachments.push(attachment)
        this.messageService.add({ severity: 'info', summary: 'Success', detail: 'Upload complete', key: 'request' });


      } catch (error: any) {
        this.uploading = false
        console.log(error)
      }
    }
  }

  async viewAttachment(attachment: any) {

    const { identityId } = await Auth.currentCredentials(); //move to Auth service
    let url
    console.log(attachment.key)
    this.blocked = true
//    if (identityId === attachment.identity) {
      //user owns attachment
      url = await Storage.get(attachment.key
        , {
          //  customPrefix: { protected: 'requests/' },
          level: 'protected', // defaults to `public`
          identityId: attachment.identity // id of another user, if `level: protected`
          // download: true, // defaults to false
          // expires?: number, // validity of the URL, in seconds. defaults to 900 (15 minutes)
          // contentType?: string // set return content type, eg "text/html"
        }
      )
    // } else {
    //   //user does not own attachment
    //   let a = await this.api.ReadAttachment({
    //     requestId: this.request.id, attachment: {
    //       fileName: attachment.fileName,
    //       fileSize: attachment.fileSize,
    //       fileType: attachment.fileType,
    //       key: attachment.key,
    //       identity: attachment.identity,
    //       url: null
    //     }
    //   })

    //   if (a.url) {
    //     url = a.url
    //   }

    // }
    console.log(url)
    if (url !== undefined && url !== '' && url !== null) {
      //   console.log((url as string).replace('public/', ''))
      // let link = (url as string).replace('private/', '') as string
      this.blocked = false;
      let link = (url as string)
      window.open(link, '_blank')
    } else {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error retrieving Attachment', key: 'request' });
      this.blocked = false;
    }
  }

  saveRequest() {
    if (this.form.status === 'VALID') {
      console.log("save request")
      this.blocked = true;
      if (this.organizationStore.organization.id !== '' &&
        this.organizationStore.organization.id !== null &&
        this.organizationStore.organization.id !== undefined
      ) {

        this.request.id = undefined
        this.request.organizationID = this.organizationStore.organization.id
        this.request.description = this.form.value.description
        this.request.companyCode = this.form.value.companyCode.code
        this.request.amount = this.form.value.amount

        //this.api.CreateRequest(this.request).then(event => {
        this.api.SubmitRequest({
          companyCode: this.request.companyCode,
          investmentType: '',
          description: this.request.description,
          amount: this.request.amount,
          organizationID: this.request.organizationID as string,
          attachments: this.request.attachments
        }).then(event => {

          console.log(event);
          this.blocked = false;
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Request created', key: 'request' });
          //    this.api.CreateRequest(this.request).then(event => {
          //      console.log("Request Created", event)
          //      let requestId = event.id;

          //   //temp - move to dedicated service
          // read workflow definition
          if (this.organizationStore.organization.workflowDefinition !== undefined && this.organizationStore.organization.workflowDefinition !== null) {
            //get levels
            //get steps
            // is step required?
            //yes
            //who are the agents?
            //create a workflow instance
            // let workflowInstance: CreateWorkflowInstanceInput = {
            //   organizationID: this.request.organizationID as string,
            //   workflowDefinitionId: this.organizationStore.organization.workflowDefinition.id as string,
            //   inProgress: 'X'
            // }
            // this.api.CreateWorkflowInstance(workflowInstance).then(event => {
            // this.api.CreateWorkflowInstanceAndSendEmail({
            //   organizationID: this.request.organizationID as string,
            //   workflowDefinitionId: this.organizationStore.organization.workflowDefinition.id as string,
            // }).then(event => {

            //   //assign definition id, request id, agents
            //   // create requestApprovers records
            //   //assign user, request id, workflow definition id, workflow instance id, step id, level id
            //   // let approver: CreateApproverInput = {
            //   //   user: 'adam.miles@iqxbusiness.com',
            //   //   organizationID: this.request.organizationID,
            //   //   approverRequestToApproveId: requestId,
            //   //   workflowInstanceId: event.id
            //   // }
            //   // this.api.CreateApprover(approver)
            // })
          }
          setTimeout(() => {
            this.router.navigate(['/myrequests'])
          }, 3500);
        }).catch((error) => {
          this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error creating request', key: 'request' });
          this.blocked = false;
          console.log(error)
        })
      }
    } else {
      //TO DO
      console.log("error")
      this.form.markAllAsTouched(); //temp
      this.blocked = false;
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Required data missing', key: 'request' });
      //   
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(Subscription => Subscription.unsubscribe())
  }
}
