import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { Subscriber } from './../API.service';
import { Amplify, Auth, Storage } from "aws-amplify";
import aws_exports from "./../../aws-exports";
import API, { GRAPHQL_AUTH_MODE } from "@aws-amplify/api-graphql";


export interface AuthControl {
  subdomain: string,
  subscriber: string
  organizationID: string
  loading: boolean
  federated: boolean
  hosted_ui_url: string
  updated: string
  invalid: boolean,
  admin: boolean
}

export const initialAuthControl: AuthControl = {
  subdomain: '',
  subscriber: '',
  organizationID: '',
  loading: true,
  federated: false,
  hosted_ui_url: '',
  updated: '',
  invalid: false,
  admin: false
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly AuthSubject = new BehaviorSubject<AuthControl>(initialAuthControl)
  readonly AuthContext$ = this.AuthSubject.asObservable()

  constructor() {
    this.loadAuth()
  }

  public refresh() {
    this.loadAuth()
  }

  private async loadAuth() {

    let hostname = location.host
    if (hostname.indexOf('localhost') === 0) {
      hostname = 'trial.capexonline.com'
    }
    let parts = hostname.split('.')
    let subdomain = parts[0]


    let auth: AuthControl = initialAuthControl;

    const readSubscriber = /* GraphQL */ `
      query ReadSubscriber($subdomain: String!) {
        getSubscriber(subdomain: $subdomain) {
         subdomain   
         organizationID
         user_pools_id
         app_client_id
         federated
         federated_domain
         redirectSignIn
       }
     }
     `;

    console.log("loading auth");

    let user = await Auth.currentCredentials()
    //console.log(user.authenticated)

    try {
      const q = await API.graphql({
        query: readSubscriber,
        variables: { subdomain: subdomain },
        authMode: user.authenticated ? 'AMAZON_COGNITO_USER_POOLS' as GRAPHQL_AUTH_MODE : 'AWS_IAM' as GRAPHQL_AUTH_MODE
      }) as any

      let res: Subscriber = q.data.getSubscriber

      //console.log("1", aws_exports)

      if (res !== null) {
        if (res.app_client_id !== '' && res.app_client_id !== null && res.app_client_id !== undefined) {
          if (res.user_pools_id !== '' && res.user_pools_id !== null && res.user_pools_id !== undefined) {
            aws_exports.aws_user_pools_id = res.user_pools_id
            aws_exports.aws_user_pools_web_client_id = res.app_client_id

            let oauth = {};

            if (res.federated && res.federated_domain !== '' && res.redirectSignIn !== '') {
              auth.federated = true
              auth.hosted_ui_url = 'https://' +
                res.federated_domain +
                '/login?client_id=' +
                res.app_client_id +
                `&response_type=code&` +
                'scope=email+openid+phone+aws.cognito.signin.user.admin+profile&' +
                `redirect_uri=` +
                res.redirectSignIn +
                '/';

              oauth = {
                domain: res.federated_domain,
                scope: ['email', 'openid', 'phone', 'aws.cognito.signin.user.admin', 'profile'],
                redirectSignIn: res.redirectSignIn + '/',
                redirectSignOut: res.redirectSignIn + '/',
                responseType: 'code',
                options: {
                  AdvancedSecurityDataCollectionFlag: false
                }
              }

            }
            Amplify.configure(aws_exports);
            Auth.configure({ oauth: oauth }) //this needed for SAML if amplify-authenticator component is used

            Storage.configure({
              AWSS3: {
                bucket: aws_exports.aws_user_files_s3_bucket,
                region: aws_exports.aws_user_files_s3_bucket_region,
                userPoolId: aws_exports.aws_user_pools_id
              }
            })

            Auth.currentAuthenticatedUser({
              bypassCache: false
            }).then((user) => {
                if (user) {
                  console.log("ok")
                  if (user.signInUserSession.accessToken.payload["cognito:groups"]) {
                    if (user.signInUserSession.accessToken.payload["cognito:groups"].find((g: String) => {
                      return g === "Admin"
                    })) {
                      auth.admin = true;
                    }
                  } else {
                    auth.admin = false;
                  }
                }
              })
              .catch(() => {
                auth.admin = false;
              });

            auth.loading = false;
            auth.subscriber = res.subdomain as string
            auth.organizationID = res.organizationID as string

            auth.updated = new Date().toString()
            this.setAuth(auth)
          }
        } else {
          auth.invalid = true
          this.setAuth(auth)
        }
      } else {
        auth.invalid = true
        this.setAuth(auth)
      }

    } catch (err) {
      auth.invalid = true
      this.setAuth(auth)
    }
  }

  get Auth(): AuthControl {
    return this.AuthSubject.getValue()
  }

  private setAuth(auth: AuthControl) {
    this.AuthSubject.next(auth)
  }


}
