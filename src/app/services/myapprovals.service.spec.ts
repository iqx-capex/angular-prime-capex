import { TestBed } from '@angular/core/testing';

import { MyapprovalsService } from './myapprovals.service';

describe('MyapprovalsService', () => {
  let service: MyapprovalsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyapprovalsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
