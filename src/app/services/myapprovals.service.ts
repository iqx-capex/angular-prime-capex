import { Injectable } from '@angular/core';
import { Auth } from "aws-amplify";
import { BehaviorSubject } from "rxjs";
import { APIService, Approver } from '../API.service';
import { AuthService } from './auth.service'

export interface MyApprovals extends Approver { }

@Injectable({
  providedIn: 'root'
})

export class MyapprovalsService {

  private readonly approvalsSubject = new BehaviorSubject<MyApprovals[]>([])
  readonly approvals$ = this.approvalsSubject.asObservable()
  private user = ''

  constructor(private api: APIService) {

    //TO DO - create a 'User' context/service instead
    Auth.currentAuthenticatedUser().then(user => {
      if (user) {
        this.user = user.username

        this.retrieveMyApprovals()

        //subscribe to when new Approvals are added for this user
        this.api.OnCreateApproverListener(this.user).subscribe((event: any) => {
          this.retrieveMyApprovals()
          //this is a bit lazy, could identify new Approval and add to approvalsSubject
        });

        //subscribe to when this user's Approvals are updated
        //(note - your Mutation must return all fields for this to work)
        this.api.OnUpdateApproverListener(this.user).subscribe((event: any) => {
//          console.log(event)
          this.retrieveMyApprovals()
          //this is a bit lazy, could identify modified request and update relevant approvalsSubject
        });
      }

    })


  }

  get approvals(): MyApprovals[] {
    return this.approvalsSubject.getValue()
  }

  retrieveMyApprovals = () => {
//    console.log(this.user)
    this.api.ApprovalsByUser(this.user).then(event => {
      // console.log(event)
      if(event){
        this.approvalsSubject.next(event.items as MyApprovals[])
      }else{
        this.approvalsSubject.next([] as MyApprovals[])
      }
     
    })
  }
}
