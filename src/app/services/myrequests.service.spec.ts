import { TestBed } from '@angular/core/testing';

import { MyrequestsService } from './myrequests.service';

describe('MyrequestsService', () => {
  let service: MyrequestsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyrequestsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
