import { Injectable } from '@angular/core';
import { Auth } from "aws-amplify";
import { BehaviorSubject } from "rxjs";
import { APIService, Request } from '../API.service';
import { AuthService } from './auth.service'

export interface MyRequests extends Request { }

@Injectable({
  providedIn: 'root'
})

export class MyrequestsService {

  private readonly approvalsSubject = new BehaviorSubject<MyRequests[]>([])
  readonly approvals$ = this.approvalsSubject.asObservable()
  private user = ''

  constructor(private api: APIService) {

    //TO DO - create a 'User' context/service instead
    Auth.currentAuthenticatedUser().then(user => {
      if (user) {
        this.user = user.username

        this.retrieveMyRequests()

        //subscribe to when new Requests are added for this user
        this.api.OnCreateRequestListener(this.user).subscribe((event: any) => {
          this.retrieveMyRequests()
          //this is a bit lazy, could identify new request and add to approvalsSubject
        });

        //subscribe to when this user's Requests are updated
        //(note - your Mutation must return all fields for this to work)
        this.api.OnUpdateRequestListener(this.user).subscribe((event: any) => {
//          console.log(event)
          this.retrieveMyRequests()
          //this is a bit lazy, could identify modified request and update relevant approvalsSubject
        });
      }

    })


  }

  get approvals(): MyRequests[] {
    return this.approvalsSubject.getValue()
  }

  retrieveMyRequests = () => {
//    console.log(this.user)
    this.api.RequestsByOwner(this.user).then(event => {
      // console.log(event)
      if(event){
        this.approvalsSubject.next(event.items as MyRequests[])
      }else{
        this.approvalsSubject.next([] as MyRequests[])
      }
     
    })
  }
}
