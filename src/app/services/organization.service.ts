import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { AuthService } from './auth.service'
import { APIService, GetOrganizationQuery } from '../API.service';
// import { API2Service, GetOrganizationWorkflowsQuery } from '../API2.service';
// import { WorkflowDefinition } from '../workflow/models/workflow'
import { UpdateOrganizationInput, CompanyCodeInput } from '../API.service'

interface OrganizationHeader {
  id: string,
  name: string
}

export interface CompanyCode extends CompanyCodeInput {
}

export interface Organization extends UpdateOrganizationInput {
  //companyCodes:CompanyCodeInput[]
}

const initialState: Organization = {
  id: '1',
  name: '',
  companyCodes: []
}

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  private readonly organizationSubject = new BehaviorSubject<Organization>(initialState)
  readonly organization$ = this.organizationSubject.asObservable()

  private readonly companyCodesSubject = new BehaviorSubject<CompanyCode[]>([])
  readonly companyCodes$ = this.companyCodesSubject.asObservable()

  private readonly loadingSubject = new BehaviorSubject<boolean>(true)
  readonly loading$ = this.loadingSubject.asObservable()

  private organizationID: string = ''

  constructor(private authService: AuthService, private api: APIService,
    // private api2: API2Service
  ) {

    this.authService.AuthContext$.subscribe(event => {

      console.log("Auth Context loaded")
      this.organizationID = event.organizationID

      if (this.organizationID !== '') {   //and check for auth user authenticated !

        this.api.ListOrganizations({ id: { eq: this.organizationID } }).then(event => {
          console.log("Org found")
          if (event.items) {
            if (event.items.length === 1) {

              let orgId = event.items[0]?.id as string

              this.api.GetOrganization(orgId).then(event => {

                let result = <GetOrganizationQuery>event;
                let organization: Organization = initialState
                organization.id = result.id
                organization.name = result.name as string

                if (!(result.companyCodes === undefined || result.companyCodes === null)) {

                  organization.companyCodes = []
                  result.companyCodes.forEach((cc) => {
                    let code: string = (cc!.code as string)
                    let description: string = (cc!.description as string)
                    if (code !== null && code !== undefined && description !== null && description !== undefined) {
                      let companyCode: CompanyCode = { code: code, description: description }
                      if (organization.companyCodes !== null && organization.companyCodes !== undefined) {
                        organization.companyCodes.push(companyCode)
                      }
                    }
                  })
                }
                // this.api2.GetOrganizationWorkflows("eb2c9c70-b0fd-4bd9-a948-63e1226a99e9").then(event => {

                //   let result = <GetOrganizationWorkflowsQuery>event;
                //   // let organization: Organization = initialState
                //   // organization.id = result.id
                //   // organization.name = result.name as string

                //   console.log("organization workflows retreived")
                //   if(result.workflowDefinition !== undefined && result.workflowDefinition !== null){
                //     organization.workflowDefinition = result.workflowDefinition
                //   }

                //   this.setOrganization(organization)
                //   this.setLoading(false)

                // })

                this.setOrganization(organization)
                this.setLoading(false)

              })
            }
          }
        })
      }
    })
  }

  get organization(): Organization {
    return this.organizationSubject.getValue()
  }

  private setOrganization(org: Organization) {
    this.organizationSubject.next(org)
    this.companyCodesSubject.next(org.companyCodes as CompanyCode[])
  }

  updateOrganizationHeader(header: OrganizationHeader) {
    let newOrganization: Organization = { ...this.organization, ...header }
    this.setOrganization(newOrganization)
  }

  updateCompanyCodes(ccs: CompanyCode[]) {
    let newOrganization: Organization = { ...this.organization }
    newOrganization.companyCodes = ccs
    this.setOrganization(newOrganization)
  }

  // updateWorkflowDefinition(def: WorkflowDefinition) {
  //   let newOrganization: Organization = { ...this.organization }
  //   newOrganization.workflowDefinition = def
  //   this.setOrganization(newOrganization)
  //   this.saveOrganization() 
  // }

  async saveOrganization(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.api.UpdateOrganization(this.organization).then(event => {
        resolve("ok")
      }).catch((error) => {
        reject(error)
      })
    })
  }

  get companyCodes(): CompanyCode[] {
    return this.companyCodesSubject.getValue()
  }

  get loading(): boolean {
    return this.loadingSubject.getValue()
  }

  private setLoading(l: boolean) {
    this.loadingSubject.next(l)
  }

}
