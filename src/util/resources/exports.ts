export const AWSResources = [
    { 
        "subdomain":"trial",
        "organizationID":"d503f98a-da23-4434-a9c9-48614caeb0ee",    //change
        "aws_user_pools_id": "ap-southeast-2_GXGv1VLxb",
        "aws_user_pools_web_client_id": "771iqnpdiee3mleahen18so3df",
        "hosted_ui":"",
        "oauth": {}
    },
    {
        "subdomain":"iqx",
        "organizationID":"d503f98a-da23-4434-a9c9-48614caeb0ee",
        "aws_user_pools_id": "ap-southeast-2_d6QPRPCjR",
        "aws_user_pools_web_client_id": "4li6nck565egevck1b91e2k3pf",
        "hosted_ui": "https://dev-iqx-capexonline.auth.ap-southeast-2.amazoncognito.com/login?client_id=4li6nck565egevck1b91e2k3pf&response_type=code&scope=email+openid+phone+aws.cognito.signin.user.admin+profile&redirect_uri=http://localhost:4200/",
        "oauth": {
            "domain": "dev-iqx-capexonline.auth.ap-southeast-2.amazoncognito.com",
            "scope": ['email', 'openid', 'phone', 'aws.cognito.signin.user.admin', 'profile'],
            "redirectSignIn": "http://localhost:4200/",
            "redirectSignOut": "http://localhost:4200/",
            "responseType": "code",
            "options": {
                "AdvancedSecurityDataCollectionFlag": false
            }
        },
    }
]

